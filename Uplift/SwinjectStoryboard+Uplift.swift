//
//  SwinjectStoryboard+Uplift.swift
//  Uplift
//
//  Created by Bryan Yuen on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import SwinjectStoryboard
import Swinject

extension SwinjectStoryboard {
  @objc class func setup() {
    // View Controllers
    defaultContainer.storyboardInitCompleted(HomeViewController.self) { _, _ in }
  }
}

