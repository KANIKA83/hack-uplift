//
//  AppNavigation.swift
//  Uplift
//
//  Created by Bryan Yuen on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import SwinjectStoryboard

class AppNavigation {
  private var appWindow: ApplicationWindow!
  private var observers: [NSObjectProtocol] = []
  
  init (appWindow window: ApplicationWindow) {
    appWindow = window
    
    initNotificationHandlers()
  }
  
  deinit {
    for observer in observers {
      NotificationCenter.default.removeObserver(observer)
    }
  }
  
  private func initNotificationHandlers() {
    let notificationCenter = NotificationCenter.default
    
//    observers.append(notificationCenter.addObserver(forName: SplashViewController.startSplashNotification,
//                                                    object: nil,
//                                                    queue: nil,
//                                                    using: { [weak self] _ in
//                                                      self?.startSplashFlow()
//    }))
  }
  
//  private func startSplashFlow() {
//    guard let splashNavController = SwinjectStoryboard.create(name: SplashViewController.storyboard, bundle: nil).instantiateInitialViewController() as? SplashViewController else {
//      return
//    }
//    DispatchQueue.main.async {
//      self.appWindow.set(rootViewController: splashNavController)
//    }
//  }
}

