//
//  UIViewController+Presentation.swift
//  Two Bulls
//
//  Created by Chen-Po Sun on 25/10/2016.
//  Copyright © 2016 Two Bulls. All rights reserved.
//

import SwinjectStoryboard
//import JGProgressHUD


protocol UINavigationDelegate: class {
  //  static var activeViewController: UIViewController? { get }
  
  var navigationController: UINavigationController? { get }
  var presentedViewController: UIViewController? { get }
  
  func present(storyBoard: String, viewControllerIdentifier: String?, animated: Bool, completion: (() -> Void)?)
  func present<T>(storyBoard: String, viewControllerIdentifier: String?, animated: Bool, transitionStyle: UIModalTransitionStyle, presentationStyle: UIModalPresentationStyle,  injection: ((T) -> Void)? , completion: (() -> Void)?)
  func present<T>(storyBoard: String, viewControllerIdentifier: String?, animated: Bool, injection: ((T) -> Void)?, completion: (() -> Void)?)
  func present<T>(storyBoard: String, viewControllerIdentifier: String?, transitionDelegate: UIViewControllerTransitioningDelegate?, animated: Bool, injection: ((T) -> Void)?, completion: (() -> Void)?)
  func push(storyBoard: String, viewControllerIdentifier: String?, animated: Bool)
  func push<T>(storyBoard: String, viewControllerIdentifier: String?, animated: Bool, injection: ((T) -> Void)?)
  func push(_ viewController: UIViewController, animated: Bool)
  func pop(animated: Bool) -> (UIViewController?)
  func popTo(viewController: UIViewController, animated: Bool) -> ([UIViewController]?)
  func dismiss(animated: Bool, completion: (() -> Void)?)
  func present(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)?)
  //  func showPopup(storyBoard: String, viewControllerIdentifier: String?, onClose: (() -> Void)?)
  //  func showPopup<T>(storyBoard: String, viewControllerIdentifier: String?, injection: ((T) -> Void)?, onClose: (() -> Void)?)
  
//  func showLoadingHUD(message: String, asBlocking blocking: Bool)
//  func showLoadingSuccess(message: String)
//  func hideLoadingHUD(afterDelay delay: TimeInterval)
//  func showProgressHUD(message: String, asBlocking blocking: Bool)
//  func setProgress(_ progress: Float, animated: Bool)
//  func setProgressComplete(message: String)
//  func hideProgressHUD(afterDelay delay: TimeInterval)
  
  func displayAlertError(title: String?, message: String?)
  func displayAlertError(title: String?, message: String?, onOkay: @escaping (UIAlertAction) -> Void)
  func displayAlertError(title: String?, message: String?, buttonString: String?,
                         onOkay: @escaping (UIAlertAction) -> Void)
  func displayConfirmationAlert(title: String?, message: String?, okText: (String), onOkay: @escaping (UIAlertAction) -> Void)
  func displayConfirmationAlert(title: String?, message: String?, okText: String, cancelText: String, onOkay: @escaping (UIAlertAction) -> Void, onCancel: @escaping (UIAlertAction) -> Void)
  func displayConfirmationAlert(title: String?, message: String?, okText: String, additionalActionText: String?, cancelText: String, onOkay: @escaping (UIAlertAction) -> Void, onAdditionalAction: ((UIAlertAction) -> Void)?, onCancel: @escaping (UIAlertAction) -> Void)
  func displayTextFieldDialog(title: String?, message: String?, textFieldPlaceholder: String?, okText: String, onOkay: @escaping (_ textFieldText: String?) -> Void)
  
  func displayTextFieldDialogCancelable(title: String?,
                                        message: String?,
                                        textFieldPlaceholder: String?,
                                        okText: String ,
                                        onOkay: @escaping (_ textFieldText: String?) -> Void,
                                        cancelText: String,
                                        onCancel: @escaping (UIAlertAction) -> Void,
                                        delegate: UITextFieldDelegate?,
                                        keyboardType: UIKeyboardType)
}

extension UINavigationDelegate {
  // Adding convenience function here to get around restrictions on default arguments for protocols
//  func showLoadingHUD(message: String) {
//    showLoadingHUD(message: message, asBlocking: true)
//  }
//  
//  func showProgressHUD(message: String) {
//    showProgressHUD(message: message, asBlocking: true)
//  }
//  
//  func hideLoadingHUD() {
//    hideLoadingHUD(afterDelay: 0)
//  }
//  
//  func hideProgressHUD() {
//    hideProgressHUD(afterDelay: 0)
//  }
}

extension UINavigationDelegate {
  func displayUnexpectedErrorAlert(code: Int?) {
    let title = "Hmmm..."
    let message = String(format:NSLocalizedString("An unexpected error has occured. Please try again.\n\n(Code: %d)", comment: "An unexpected error has occured. Please try again.\n\n(Code: %d)"), code ?? -1)
    self.displayAlertError(title: title, message: message)
  }
}

private var loadingHudKey: UInt8 = 0
private var progressHudKey: UInt8 = 0
private var actionToEnableKey: UInt8 = 0

extension UIViewController: UINavigationDelegate {
  
  /**
   Present (modally) view controller from given storyboard.
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter: animated: Present with animation
   - parameter: completion: Closure when present is completed
   */
  func present(storyBoard: String, viewControllerIdentifier: String? = nil, animated: Bool = true, completion: (() -> Void)? = nil) {
    assert(Thread.isMainThread, "UIKit code must run on main thread!")
    
    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
    var viewController : UIViewController?
    
    if let viewControllerIdentifier = viewControllerIdentifier {
      viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
    } else {
      viewController = storyBoard.instantiateInitialViewController()
    }
    
    assert(viewController != nil)
    
    if let viewController = viewController {
      present(viewController, animated: animated, completion: completion)
    }
  }
  
  func present<T>(storyBoard: String, viewControllerIdentifier: String?, animated: Bool, transitionStyle: UIModalTransitionStyle = .coverVertical, presentationStyle: UIModalPresentationStyle = .fullScreen,  injection: ((T) -> Void)?, completion: (() -> Void)?) {
    assert(Thread.isMainThread, "UIKit code must run on main thread!")
    
    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
    var viewController : UIViewController?
    
    if let viewControllerIdentifier = viewControllerIdentifier {
      viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
    } else {
      viewController = storyBoard.instantiateInitialViewController()
    }
    
    assert(viewController != nil)
    
    viewController?.modalTransitionStyle = transitionStyle
    viewController?.modalPresentationStyle = presentationStyle
    
    if let viewController = viewController {
      if let navigationController = viewController as? UINavigationController {
        injection?(navigationController.viewControllers.first as! T)
      } else {
        injection?(viewController as! T)
      }
      present(viewController, animated: animated, completion: completion)
    }
  }
  
  /**
   Present (modally) view controller from given storyboard.
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter: animated: Present with animation
   - parameter: injection: Closure to inject view model into view
   - parameter: completion: Closure when present is completed
   */
  func present<T>(storyBoard: String, viewControllerIdentifier: String? = nil, animated: Bool = true, injection: ((T) -> Void)? = nil, completion: (() -> Void)? = nil) {
    present(storyBoard: storyBoard, viewControllerIdentifier: viewControllerIdentifier, transitionDelegate: nil, animated: true, injection: injection, completion: completion)
  }
  
  /**
   Present (modally) view controller from given storyboard.
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter transitionDelegate: transitioning delegate for returning the custom animation controller
   - parameter: animated: Present with animation
   - parameter: injection: Closure to inject view model into view
   - parameter: completion: Closure when present is completed
   */
  func present<T>(storyBoard: String, viewControllerIdentifier: String? = nil, transitionDelegate: UIViewControllerTransitioningDelegate? = nil, animated: Bool = true, injection: ((T) -> Void)? = nil, completion: (() -> Void)? = nil) {
    assert(Thread.isMainThread, "UIKit code must run on main thread!")
    
    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
    var viewController : UIViewController?
    
    if let viewControllerIdentifier = viewControllerIdentifier {
      viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
    } else {
      viewController = storyBoard.instantiateInitialViewController()
    }
    
    assert(viewController != nil)
    
    if let viewController = viewController {
      viewController.transitioningDelegate = transitionDelegate
      if let navigationController = viewController as? UINavigationController {
        injection?(navigationController.viewControllers.first as! T)
      } else {
        injection?(viewController as! T)
      }
      
      present(viewController, animated: animated, completion: completion)
    }
  }
  
  /**
   Push view controller from given storyboard.
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter: animated: Present with animation
   - parameter: completion: Closure when present is completed
   */
  func push(storyBoard: String, viewControllerIdentifier: String? = nil, animated: Bool = true) {
    assert(navigationController != nil)
    
    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
    var viewController : UIViewController?
    
    if let viewControllerIdentifier = viewControllerIdentifier {
      viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
    } else {
      viewController = storyBoard.instantiateInitialViewController()
    }
    
    assert(viewController != nil)
    
    if let viewController = viewController {
      navigationController?.pushViewController(viewController, animated: animated)
    }
  }
  
  /**
   Push view controller from given storyboard.
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter: animated: Present with animation
   - parameter: injection: Closure to inject view model into view
   - parameter: completion: Closure when present is completed
   */
  func push<T>(storyBoard: String, viewControllerIdentifier: String? = nil, animated: Bool = true, injection: ((T) -> Void)? = nil) {
    assert(navigationController != nil)
    
    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
    var viewController : T?
    
    if let viewControllerIdentifier = viewControllerIdentifier {
      viewController = storyBoard.instantiateViewController(withIdentifier:viewControllerIdentifier) as? T
    } else {
      viewController = storyBoard.instantiateInitialViewController() as? T
    }
    
    assert(viewController != nil)
    
    if let viewController = viewController {
      injection?(viewController)
      navigationController?.pushViewController(viewController as! UIViewController, animated: animated)
    }
  }
  
  /**
   Present view controller modally in a custom `ModalPopupViewController`
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter: onClose: Closure to execute when closing the modal popup
   */
  //  func showPopup(storyBoard: String, viewControllerIdentifier: String?, onClose: (() -> Void)?) {
  //    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
  //    var viewController: UIViewController?
  //
  //    if let viewControllerIdentifier = viewControllerIdentifier {
  //      viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
  //    } else {
  //      viewController = storyBoard.instantiateInitialViewController()
  //    }
  //
  //    assert(viewController != nil)
  //
  //    if let viewController = viewController {
  //      ModalPopupViewController.present(viewController, from: self, onClose: onClose)
  //    }
  //  }
  
  /**
   Present view controller modally in a custom `ModalPopupViewController`
   - parameter: storyBoard: Name of storyboard
   - parameter: viewControllerIdentifier: Identifier of view controller to present. If nil, will present initial view controller of storyboard
   - parameter: injection: Closure to inject view model into view
   - parameter: onClose: Closure to execute when closing the modal popup
   */
  //  func showPopup<T>(storyBoard: String, viewControllerIdentifier: String?, injection: ((T) -> Void)?, onClose: (() -> Void)?) {
  //    let storyBoard = SwinjectStoryboard.create(name: storyBoard, bundle: nil, container: SwinjectStoryboard.defaultContainer)
  //    var viewController: T?
  //
  //    if let viewControllerIdentifier = viewControllerIdentifier {
  //      viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier) as? T
  //    } else {
  //      viewController = storyBoard.instantiateInitialViewController() as? T
  //    }
  //
  //    assert(viewController != nil)
  //
  //    if let viewController = viewController {
  //      injection?(viewController)
  //      ModalPopupViewController.present(viewController as! UIViewController, from: self, onClose: onClose)
  //    }
  //  }
  
  /**
   Push a view controller.
   - parameter: viewController: Vview controller to present.
   - parameter: animated: Present with animation
   */
  func push(_ viewController: UIViewController, animated: Bool = true) {
    navigationController?.pushViewController(viewController, animated: animated)
  }
  
  /**
   Pop the topmost view controller from the current navigation controller
   - return: Returns the popped view controller.
   */
  func pop(animated: Bool = true) -> UIViewController? {
    return navigationController?.popViewController(animated: animated)
  }
  
  /**
   Pops view controllers until the one specified is on top.
   - parameter: viewController: The viewController to be on top
   - return: Returns the popped controllers.
   */
  func popTo(viewController: UIViewController, animated: Bool = true) -> [UIViewController]? {
    return navigationController?.popToViewController(viewController, animated: animated)
  }
  
//  /**
//   Show loading spinner
//   - parameter message: String to show under the spinner
//   - parameter asBlocking: Whether or not the HUB is completely blocking (ie blocks nav and tab bars) Default it true
//   */
//  func showLoadingHUD(message: String, asBlocking blocking: Bool = true) -> Void {
//    // Hid other HUD
//    hideProgressHUD()
//
//    loadingHud.textLabel.text = message
//    loadingHud.show(in: blocking ? view.window! : view)
//  }
//
//  /**
//   */
//  func showLoadingSuccess(message: String) {
//    loadingHud.textLabel.text = message
//    loadingHud.indicatorView = JGProgressHUDSuccessIndicatorView()
//  }
//
//  /**
//   Hide loading spinner
//   - parameter delay: The delay until the HUD will be dismissed.
//   */
//  func hideLoadingHUD(afterDelay delay: TimeInterval = 0) -> Void {
//    loadingHud.dismiss(afterDelay: delay)
//  }
//
//  /**
//   Show progress spinner
//   - parameter: message: String to show under the spinner
//   - parameter: asBlocking: Whether or not the HUB is completely blocking (ie blocks nav and tab bars) Default it true
//   */
//  func showProgressHUD(message: String, asBlocking blocking: Bool = true) -> Void {
//    // Hid other HUD
//    hideLoadingHUD()
//
//    progressHud.textLabel.text = message
//    progressHud.indicatorView = JGProgressHUDRingIndicatorView.init()
//    progressHud.show(in: blocking ? view.window! : view)
//  }
//
//  func setProgress(_ progress: Float, animated: Bool) {
//    progressHud.indicatorView?.setProgress(progress, animated: animated)
//  }
//
//  /**
//   */
//  func setProgressComplete(message: String) {
//    progressHud.textLabel.text = message
//    progressHud.indicatorView = JGProgressHUDSuccessIndicatorView()
//  }
  
//  /**
//   Hide progress spinner
//   */
//  func hideProgressHUD(afterDelay delay: TimeInterval = 0) -> Void {
//    progressHud.dismiss(afterDelay: delay)
//  }
  
  /**
   Display an alert
   - parameter: title: Title string
   - parameter: message: Body string
   */
  func displayAlertError(title: String?, message: String?) -> Void {
    popUpError(title: title, message: message, onOkay: nil)
  }
  
  /**
   Display an alert
   - parameter: title: Title string
   - parameter: message: Body string
   - parameter: onOkay: Closure when OK is pressed
   */
  func displayAlertError(title: String?, message: String?, onOkay: @escaping (UIAlertAction) -> Void) {
    popUpError(title: title, message: message, onOkay: onOkay)
  }
  
  /**
   Display an alert
   - parameter: title: Title string
   - parameter: message: Body string
   - parameter: buttonString: Button String
   - parameter: onOkay: Closure when OK is pressed
   */
  func displayAlertError(title: String?, message: String?, buttonString: String?,
                         onOkay: @escaping (UIAlertAction) -> Void) {
    popUpError(title: title, message: message, buttonString: buttonString,
               onOkay: onOkay)
  }
  
  func displayConfirmationAlert(title: String?, message: String?, okText: String = NSLocalizedString("OK", comment: "OK"), onOkay: @escaping (UIAlertAction) -> Void) {
    popUpConfirmAlert(title: title, message: message, okText: okText, cancelText: NSLocalizedString("Cancel", comment: "Cancel"), onOkay: onOkay, onCancel: { _ in })
  }
  
  // It would be nice to use optional parameters for escaping blocks (with default values to nil) to consolidate the above method with this method.
  // However, swift bug: https://bugs.swift.org/browse/SR-2444
  func displayConfirmationAlert(title: String?,
                                message: String?,
                                okText: String = NSLocalizedString("OK", comment: "OK"),
                                cancelText: String = NSLocalizedString("Cancel", comment: "Cancel"),
                                onOkay: @escaping (UIAlertAction) -> Void,
                                onCancel: @escaping (UIAlertAction) -> Void) {
    popUpConfirmAlert(title: title, message: message, okText: okText, cancelText: cancelText, onOkay: onOkay, onCancel: onCancel)
  }
  
  func displayConfirmationAlert(title: String?,
                                message: String?,
                                okText: String = NSLocalizedString("OK", comment: "OK"),
                                additionalActionText: String? = nil,
                                cancelText: String = NSLocalizedString("Cancel", comment: "Cancel"),
                                onOkay: @escaping (UIAlertAction) -> Void,
                                onAdditionalAction: ((UIAlertAction) -> Void)? = nil,
                                onCancel: @escaping (UIAlertAction) -> Void) {
    popUpConfirmAlert(title: title, message: message, okText: okText, additionalActionText: additionalActionText, cancelText: cancelText, onOkay: onOkay, onAdditionalAction: onAdditionalAction, onCancel: onCancel)
  }
  /**
   Display alert with a text field.
   'OK' button will be disabled until text is entered
   */
  func displayTextFieldDialog(title: String?,
                              message: String?,
                              textFieldPlaceholder: String?,
                              okText: String = NSLocalizedString("OK", comment: "OK"),
                              onOkay: @escaping (_ textFieldText: String?) -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addTextField(configurationHandler: { textField in
      textField.placeholder = textFieldPlaceholder
      textField.addTarget(self, action: #selector(self.textFieldDialogTextChanged), for: .editingChanged)
    })
    let okAction = UIAlertAction(title: okText, style: .default, handler: { _ in
      onOkay(alert.textFields![0].text)
    })
    alert.addAction(okAction)
    actionToEnable = okAction
    actionToEnable.isEnabled = false
    
    present(alert, animated: true, completion: nil)
  }
  
  /**
   Display alert with a text field.
   */
  func displayTextFieldDialogCancelable(title: String?,
                                        message: String?,
                                        textFieldPlaceholder: String?,
                                        okText: String = NSLocalizedString("OK", comment: "OK"),
                                        onOkay: @escaping (_ textFieldText: String?) -> Void,
                                        cancelText: String = NSLocalizedString("Cancel", comment: "Cancel"),
                                        onCancel: @escaping (UIAlertAction) -> Void,
                                        delegate: UITextFieldDelegate?,
                                        keyboardType: UIKeyboardType) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addTextField(configurationHandler: { textField in
      textField.placeholder = textFieldPlaceholder
      textField.addTarget(self, action: #selector(self.textFieldDialogTextChanged), for: .editingChanged)
      textField.keyboardType = keyboardType
      textField.autocapitalizationType = .words
      if let delegate = delegate {
        textField.delegate = delegate
      }
    })
    let okAction = UIAlertAction(title: okText, style: .default, handler: { _ in
      onOkay(alert.textFields![0].text)
    })
    alert.addAction(okAction)
    actionToEnable = okAction
    actionToEnable.isEnabled = false
    
    let cancelAction = UIAlertAction(title: cancelText, style: .cancel, handler: onCancel)
    alert.addAction(cancelAction)
    
    present(alert, animated: true, completion: nil)
  }
  
  private var actionToEnable: UIAlertAction {
    get { return associatedObject(base: self, key: &actionToEnableKey) { return UIAlertAction.init() } }
    set { associateObject(base: self, key: &actionToEnableKey, value: newValue) }
  }
  
  @objc func textFieldDialogTextChanged(_ sender: Any) {
    let tf = sender as! UITextField
    actionToEnable.isEnabled = (tf.text != "")
  }
  
  private func popUpError(title: String?, message: String?,
                          buttonString: String? = "OK", onOkay: ((UIAlertAction) -> Void)?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString(buttonString!, comment: buttonString!), style: .default, handler: onOkay))
    present(alert, animated: true, completion: nil)
  }
  
  private func popUpConfirmAlert(title: String?,
                                 message: String?,
                                 okText : String,
                                 additionalActionText: String? = nil,
                                 cancelText: String,
                                 onOkay: @escaping (UIAlertAction) -> Void,
                                 onAdditionalAction: ((UIAlertAction) -> Void)? = nil,
                                 onCancel: @escaping (UIAlertAction) -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: onCancel))
    alert.addAction(UIAlertAction(title: okText, style: .default, handler: onOkay))
    if additionalActionText != nil {
      alert.addAction(UIAlertAction(title: additionalActionText, style: .default, handler: onAdditionalAction))
    }
    present(alert, animated: true, completion: nil)
  }
  
//  private var loadingHud: JGProgressHUD {
//    get { return associatedObject(base: self, key: &loadingHudKey) { return JGProgressHUD.init(style: .dark) } }
//    set { associateObject(base: self, key: &loadingHudKey, value: newValue) }
//  }
//
//  private var progressHud: JGProgressHUD {
//    get {
//      return associatedObject(base: self, key: &progressHudKey) {
//        let hud = JGProgressHUD.init(style: .dark)
//        hud.indicatorView = JGProgressHUDRingIndicatorView.init()
//        return hud
//      }
//    }
//    set { associateObject(base: self, key: &progressHudKey, value: newValue) }
//  }
  
  /**
   Implement extension properties using Objective-C associated objects
   */
  private func associatedObject<ValueType: AnyObject>(base: AnyObject, key: UnsafePointer<UInt8>, initialiser: () -> ValueType) -> ValueType {
    if let associated = objc_getAssociatedObject(base, key) as? ValueType { return associated }
    
    let associated = initialiser()
    objc_setAssociatedObject(base, key, associated, .OBJC_ASSOCIATION_RETAIN)
    return associated
  }
  
  private func associateObject<ValueType: AnyObject>(base: AnyObject, key: UnsafePointer<UInt8>, value: ValueType) {
    objc_setAssociatedObject(base, key, value, .OBJC_ASSOCIATION_RETAIN)
  }
}
