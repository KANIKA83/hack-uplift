//
//  PhotoPickerConfig.swift
//  Uplift
//
//  Created by Bryan Yuen on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import YPImagePicker
import AVFoundation
import UIKit

func getPhotoPickerConfig(_ pickerScreens: [YPPickerScreen]) -> YPImagePickerConfiguration {
  var config = YPImagePickerConfiguration()
  config.library.mediaType = .photo
  config.library.onlySquare  = false
  config.onlySquareImagesFromCamera = true
  config.targetImageSize = .original
  config.usesFrontCamera = false
  config.showsFilters = true
  config.filters = [YPFilter(name: "Normal", coreImageFilterName: ""),
                    YPFilter(name: "Mono", coreImageFilterName: "CIPhotoEffectMono"),
                    YPFilter(name: "Blur", coreImageFilterName: "CIBoxBlur")]
  config.shouldSaveNewPicturesToAlbum = true
  config.video.compression = AVAssetExportPresetHighestQuality
  config.albumName = "Uplift"
  config.screens = pickerScreens
//  [.library, .photo, .video]
  config.startOnScreen = pickerScreens.first!
  config.video.recordingTimeLimit = 10
  config.video.libraryTimeLimit = 20
//  config.showsCrop = .rectangle(ratio: (16/9))
  config.wordings.libraryTitle = "Moment"
  config.wordings.cameraTitle = "Capture your moment"
  config.wordings.videoTitle = "Record your moment"
  config.hidesStatusBar = false
  config.library.maxNumberOfItems = 1
  config.library.minNumberOfItems = 1
  config.library.numberOfItemsInRow = 3
  config.library.spacingBetweenItems = 2
  config.isScrollToChangeModesEnabled = false
  
  return config
}

func setupPhotoPicker() {
  
}
