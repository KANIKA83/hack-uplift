//
//  ViewControllerPhotoView.swift
//  Uplift
//
//  Created by Gavin Davidson on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class ViewControllerPhotoView: UIViewController {
  static let storyboard = "PhotoView"
  static let identifier = "ViewControllerPhotoView"
  
  var viewModel: PhotoViewViewModel!
  var captionToSave = Variable<String>("")
  
  private let disposeBag = DisposeBag()
  
  @IBOutlet weak var PostButton: UIButton!
  @IBOutlet weak var selectedImageView: UIImageView!
  
  @IBOutlet weak var captionOverlayView: UIView!
  @IBOutlet weak var captionLabel: UILabel!
  @IBOutlet weak var CaptionTextView: UITextView!
  @IBOutlet weak var Button: UIButton!
  @IBOutlet weak var backButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let purpleColour = UIColor(red: 243/255, green: 155/255, blue: 167/255, alpha: 1).cgColor
    let pinkColour = UIColor(red: 160/255, green: 145/255, blue: 222/255, alpha: 1).cgColor
    
    self.PostButton.applyGradient(colors: [purpleColour, pinkColour])
    selectedImageView.image = viewModel.image
    
    hideKeyboardWhenTappedAround()
    
    setupRX()
//    let paddingView = UIView(frame: CGRect(0, 0, 15, self.CaptionTextField.frame.height))
    
//    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.CaptionTextField.frame.height))
//    self.CaptionTextField.leftView = paddingView
//    self.CaptionTextField.leftViewMode = UITextField.ViewMode.always
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  private func setupRX() {
    CaptionTextView.rx.text.orEmpty
      .bind(to: viewModel.caption)
      .disposed(by: disposeBag)
    
    viewModel.caption.asObservable().subscribe(onNext: { [captionToSave, captionLabel, captionOverlayView] cap in
      if cap.trimmingCharacters(in: .whitespaces).isEmpty {
        captionOverlayView?.isHidden = true
      } else {
        captionOverlayView?.isHidden = false
        captionToSave.value = "\"" + cap.trimmingCharacters(in: .whitespaces) + "\""
        captionLabel?.text = captionToSave.value
      }
    }).disposed(by: disposeBag)
    
    Button.rx.tap.subscribe(onNext: { [weak self, viewModel, captionToSave, homeImages, imageCaptions] in
      homeImages.value.insert(cropToBounds(image: (viewModel?.image)!, width: Double(image1Width), height: Double(image1Height)), at: 0)
      imageCaptions.value.insert(captionToSave.value, at: 0)
      self?.dismiss(animated: true, completion: nil)
    }).disposed(by: disposeBag)
    
    backButton.rx.tap.subscribe(onNext: { [weak self] in
      self?.dismiss(animated: true, completion: nil)
    }).disposed(by: disposeBag)
  }
}

