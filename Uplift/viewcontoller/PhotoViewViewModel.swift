//
//  PhotoViewViewModel.swift
//  Uplift
//
//  Created by Bryan Yuen on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import UIKit
import RxSwift

class PhotoViewViewModel {
  var image: UIImage
  let caption: Variable<String> = Variable("")
  
  init(_ image: UIImage) {
    self.image = image
  }
}
