//
//  ViewControllerShare.swift
//  Uplift
//
//  Created by Gavin Davidson on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import UIKit

class ViewControllerShare: UIViewController {
  
  @IBOutlet weak var ShareButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let purpleColour = UIColor(red: 243/255, green: 155/255, blue: 167/255, alpha: 1).cgColor
    let pinkColour = UIColor(red: 160/255, green: 145/255, blue: 222/255, alpha: 1).cgColor
    
    self.ShareButton.applyGradient(colors: [purpleColour, pinkColour])
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  
}
