//
//  HomeViewController.swift
//  Uplift
//
//  Created by Bryan Yuen on 7/12/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import YPImagePicker

var homeImages = Variable<[UIImage?]>([nil, nil, nil, nil])
var imageCaptions = Variable<[String]>(["", "", "", ""])
var image1Width: CGFloat = 0.0
var image1Height: CGFloat = 0.0
var image2Width: CGFloat = 0.0
var image2Height: CGFloat = 0.0
var image3Width: CGFloat = 0.0
var image3Height: CGFloat = 0.0
var image4Width: CGFloat = 0.0
var image4Height: CGFloat = 0.0

class HomeViewController: UIViewController {
  static let storyboard = "Home"
  static let identifier = "HomeViewController"
  
  private var picker: YPImagePicker!
  private var selectedPhoto = Variable<YPMediaPhoto?>(nil)
  private var imageViews: [UIImageView]!
  
  private let disposeBag = DisposeBag()
  
  @IBOutlet private weak var cameraButton: UIButton!
  @IBOutlet private weak var photoPickerButton: UIButton!
  @IBOutlet private weak var captionButton: UIButton!
  @IBOutlet weak var imageView1: UIImageView!
  @IBOutlet weak var imageView2: UIImageView!
  @IBOutlet weak var imageView3: UIImageView!
  @IBOutlet weak var imageView4: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    image1Width = imageView1.frame.width
    image2Width = imageView2.frame.width
    image3Width = imageView3.frame.width
    image4Width = imageView4.frame.width
    image1Height = imageView1.frame.height
    image2Height = imageView2.frame.height
    image3Height = imageView3.frame.height
    image4Height = imageView4.frame.height
    
    setupRX()
    homeImages = Variable<[UIImage?]>([cropToBounds(image: UIImage(named: "image1")!, width: Double(imageView1.frame.width), height: Double(imageView1.frame.height)),
                                       cropToBounds(image: UIImage(named: "image2")!, width: Double(imageView2.frame.width), height: Double(imageView2.frame.height)),
                                       cropToBounds(image: UIImage(named: "image3")!, width: Double(imageView3.frame.width), height: Double(imageView3.frame.height)), nil])
    imageViews = [imageView1, imageView2, imageView3, imageView4]
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupHomeImages()
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  private func setupRX() {
    cameraButton.rx.tap.subscribe(onNext: { [weak self] in
      self?.setupPhotoPicker(.photo)
    }).disposed(by: disposeBag)
    
    photoPickerButton.rx.tap.subscribe(onNext: { [weak self] _ in
      self?.setupPhotoPicker(.library)
    }).disposed(by: disposeBag)
    
    captionButton.rx.tap.subscribe(onNext: { [weak self] in
      
    }).disposed(by: disposeBag)
    
  }
  
  private func setupPhotoPicker(_ pickerScreen: YPPickerScreen) {
    picker = YPImagePicker(configuration: getPhotoPickerConfig([pickerScreen]))
    picker.didFinishPicking { [picker, selectedPhoto] items, cancel in
      if let photo = items.singlePhoto {
        selectedPhoto.value = photo
        print(photo.fromCamera) // Image source (camera or library)
        print(photo.image) // Final image selected by the user
        print(photo.originalImage) // original image selected by the user, unfiltered
        print(photo.modifiedImage) // Transformed image, can be nil
        print(photo.exifMeta) // Print exif meta data of original image.
      }
      
      if cancel {
        picker?.dismiss(animated: true, completion: nil)
      }
      
      picker?.dismiss(animated: true) { [weak self, selectedPhoto] in
        self?.present(storyBoard: ViewControllerPhotoView.storyboard,
                viewControllerIdentifier: ViewControllerPhotoView.identifier,
                animated: true,
                transitionStyle: .crossDissolve,
                injection: { [selectedPhoto] (vc: ViewControllerPhotoView) in
                  vc.viewModel = PhotoViewViewModel((selectedPhoto.value?.image)!)
        },
                completion: nil)
      }
    }
    present(picker, animated: true, completion: nil)
  }
  
  private func setupHomeImages() {
    for index in 0...(imageViews.count - 1) {
      if let image = homeImages.value[index] {
        imageViews[index].image = image
      }
    }
  }
}
