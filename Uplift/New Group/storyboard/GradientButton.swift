
import UIKit

extension UIButton
{
  func applyGradient(colors: [CGColor])
  {
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = colors
    gradientLayer.startPoint = CGPoint(x: 0, y: 0)
    gradientLayer.endPoint = CGPoint(x: 0, y: 1)
    gradientLayer.frame = self.bounds
    self.layer.addSublayer(gradientLayer)
  }
}
